<?php
/**
 * 管理后台登录验证中间件
 * @var [type]
 */

namespace App\Http\Middleware;

use Closure;

class AdminAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //检查是否登录
        if (!$request->session()->get('x-account') || !$request->session()->get('x-user-id')) {
            return redirect('/admin/login');
        }
        return $next($request);
    }
}
