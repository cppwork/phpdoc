<?php
/**
 * 文档表模型
 * User: dengqihua
 * Date: 2017-06-28
 * Time: 15:44
 */
namespace App\Http\Model;

use Illuminate\Database\Eloquent\Model;

class Documents extends Model
{
    const CREATED_AT = 'create_time';
    const UPDATED_AT = 'update_time';

    protected $table = 'documents';

    /**
     * 获取关联的内容
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function getContent()
    {
        return $this->hasOne('App\Http\Model\DocumentsContent', 'id');
    }

    /**
     * 获取单条数据
     * @param $condition
     * @return mixed
     */
    public function getData($condition)
    {
        return $this->where($condition)->firstOrFail();
    }

    /**
     * 添加数据
     * @param $data
     * @return mixed
     */
    public function addData($data)
    {
        $data['create_time'] = $data['update_time'] = date('Y-m-d H:i:s');
        return $this->insertGetId($data);
    }

    /**
     * 更新数据
     * @param array $data
     * @param array $where
     * @return bool|mixed
     */
    public function updateData($data, $where)
    {
        $data['update_time'] = date('Y-m-d H:i:s');
        return $this->where($where)->update($data);
    }
}