<?php
/**
 * 文档与分词id关联表模型
 * User: dengqihua
 * Date: 2017-06-28
 * Time: 15:44
 */
namespace App\Http\Model;

use Illuminate\Database\Eloquent\Model;

class DocumentsParticipleId extends Model
{

    protected $table = 'documents_participle_id';
}