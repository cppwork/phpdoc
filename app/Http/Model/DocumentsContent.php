<?php
/**
 * 文档内容表模型
 * User: dengqihua
 * Date: 2017-12-16
 * Time: 14:00
 */

namespace App\Http\Model;

use Illuminate\Database\Eloquent\Model;

class DocumentsContent extends Model
{
    protected $table = 'documents_content';
    public $timestamps = false;

}
