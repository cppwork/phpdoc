<?php
/**
 * Created by PhpStorm.
 * User: dengqihua
 * Date: 2017/5/29
 * Time: 上午9:27
 */
namespace App\Http\Model;

use Illuminate\Database\Eloquent\Model;

class UserLoginLog extends Model
{
    protected $table = 'user_login_log';
    public $timestamps = false;

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /**
     * 添加登录记录
     * @param $user_id
     * @param $ip
     * @param int $type
     * @return static
     */
    public function addUserLoginLog($user_id, $ip, $type = 1)
    {
        $loginData = [
            'user_id' => $user_id,
            'ip' => $ip,
            'type' => $type,
            'login_at' => date('Y-m-d H:i:s', time()),
        ];
        return $this->insertGetId($loginData);
    }

    /**
     * 注销，更新最后一条登录记录
     * @param $user_id
     * @return mixed
     */
    public function updateUserLoginLogLast($user_id)
    {
        $model = $this->where('user_id', $user_id)->orderBy('id', 'desc')->first();
        $model->logout_at = date('Y-m-d H:i:s');
        $model->save();
    }
}
