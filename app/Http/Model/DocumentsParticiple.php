<?php
/**
 * 文档分词表模型
 * User: dengqihua
 * Date: 2017-06-28
 * Time: 15:44
 */
namespace App\Http\Model;

use Illuminate\Database\Eloquent\Model;

class DocumentsParticiple extends Model
{
    const CREATED_AT = 'create_time';
    const UPDATED_AT = 'update_time';

    protected $table = 'documents_participle';
}