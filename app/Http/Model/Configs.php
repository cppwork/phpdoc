<?php
/**
 * Created by PhpStorm.
 * User: dengqihua
 * Date: 2017/5/29
 * Time: 上午9:27
 */

namespace App\Http\Model;

use Illuminate\Database\Eloquent\Model;

class Configs extends Model
{
    protected $table = 'configs';

    const CREATED_AT = 'create_time';
    const UPDATED_AT = 'update_time';

}
