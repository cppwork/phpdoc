<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class WebhooksController extends Controller
{
    protected $request;

    public function __construct(Request $request)
    {
        parent::__construct();


        $this->request = $request;
    }

    public function gitee()
    {
        $input = json_decode($this->request->getContent(), true);
        if ($input && ($input['password'] == env('WEBHOOKS_GITEE_PWD'))) {
            $sh_path = app_path() . '/Console/Sh/webhooksGitee.sh';
            exec("sudo sh {$sh_path}", $output, $return_var);
            dd($output, $return_var);
        } else {
            abort('404');
        }
    }
}
