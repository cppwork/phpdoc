<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Model\Documents;
use App\Http\Model\DocumentsParticiple;
use App\Http\Model\DocumentsParticipleId;
use Phpanalysis\Phpanalysis;

class DocumentParticiple extends Command
{

    protected $phpanalysisServer;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'participle:execute';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '建立文档分词';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $documentModel = new Documents();

        $count = $documentModel->count();
        $limit = 100;

        $progressHandle = $this->output->createProgressBar($count);


        $documentParticipleModel   = new DocumentsParticiple();
        $documentParticipleIdModel = new DocumentsParticipleId();

        $this->phpanalysisServer = new PhpAnalysis('utf-8', 'utf-8', true);


        $pages = ceil($count / $limit);
        for ($i = 1; $i <= $pages; $i++) {
            $offset = ($i - 1) * $limit;
            $list   = $documentModel->limit($limit)->offset($offset)->get(['id', 'title', 'keyword'])->toArray();
            foreach ($list as $key => $val) {
                $document_id = $val['id'];

                $reg_list = $this->phpanalysis($val['title'] . ' ' . $val['keyword']);
                if (!$reg_list) {
                    continue;
                }
                foreach ($reg_list as $k => $v) {
                    $now_time            = date('Y-m-d H:i:s');
                    $data                = $where = ['word' => $v];
                    $data['create_time'] = $data['update_time'] = $now_time;

                    //插入数据到文档分词表
                    $participle_id = $documentParticipleModel->where($where)->value('id');
                    !$participle_id && ($participle_id = $documentParticipleModel->insertGetId($data));

                    //插入数据到分词与文档关联表
                    $data = $where = ['document_id' => $document_id, 'participle_id' => $participle_id];
                    if (!$documentParticipleIdModel->where($where)->count()) {
                        $documentParticipleIdModel->insertGetId($data);
                    }
                }
            }

            $progressHandle->advance($limit);
        }

        $progressHandle->finish();
    }

    public function phpanalysis($seg)
    {
        //执行分词
        $this->phpanalysisServer->SetSource($seg);

        //多元切分
        $this->phpanalysisServer->differMax = true;

        //新词识别
        $this->phpanalysisServer->unitWord = true;

        //开始分析
        $this->phpanalysisServer->StartAnalysis(true);

        //返回分词结果集
        $reg_list = $this->phpanalysisServer->GetFinallyResult(' ', false);

        //结果集转换数组，去除空格，重新索引
        $reg_list && $reg_list = array_values(array_unique(array_filter(explode(' ', filter_mark(strtolower($reg_list))))));

        return $reg_list;
    }
}
