# -*- coding: utf-8 -*-

from time import ctime, sleep
import threading, logging

# 开启日志
logger = logging.getLogger()
# 指定logger输出格式
formatter = logging.Formatter('%(asctime)s %(levelname)-8s: %(message)s')

# 文件日志
logfilename = 'test.log'
file_handler = logging.FileHandler(filename=logfilename, encoding="utf-8")
# 可以通过setFormatter指定输出格式
file_handler.setFormatter(formatter)
# 为logger添加的日志处理器
logger.addHandler(file_handler)
# 指定日志的最低输出级别，默认为WARN级别
logger.setLevel(logging.INFO)


def music(name):
    for i in range(2):
        logger.info(u'我在听音乐：' + name)
        sleep(1)


def move(name):
    for i in range(2):
        logger.info(u'我在看电影：' + name)
        sleep(2)


threads = []

t1 = threading.Thread(target=music, args=(u'爱情买卖',))
threads.append(t1)

t2 = threading.Thread(target=move, args=(u'阿凡达',))
threads.append(t2)

if __name__ == '__main__':
    for t in threads:
        t.setDaemon(True)
        t.start()

    t.join()
    logger.info('所有事情完成.')
