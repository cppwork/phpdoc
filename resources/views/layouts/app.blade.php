<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="keywords" content="PHP手册,PHP中文手册,PHP中文参考手册,PHP7手册,PHP7中文手册,PHP7中文参考手册,PHP最新手册,PHP最新中文手册,PHP最新中文参考手册">
    <meta name="baidu-site-verification" content="KkdffVUonZ"/>
    <title>@yield('title') - PHP中文手册</title>
    <link rel="stylesheet" href="//cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css">
    @if(env('APP_DEBUG'))
        <link rel="stylesheet" href="/css/all.css?time={{time()}}">
    @else
        <link rel="stylesheet" href="/css/all.min.css">
    @endif
    <link rel="stylesheet" href="//apps.bdimg.com/libs/nprogress/0.2.0/nprogress.min.css">

    <script src="//apps.bdimg.com/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="//cdn.bootcss.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="//cdn.bootcss.com/jquery.pjax/2.0.1/jquery.pjax.min.js"></script>
    <script src="//apps.bdimg.com/libs/nprogress/0.2.0/nprogress.min.js"></script>
</head>
<body>

<div class="container-fluid" style="margin-bottom: 60px;">

    <div class="" id="pjax-container">
        <div class="phpdoc-nav-top">
            <nav class="navbar navbar-default navbar-fixed-top">
                <div class="manualnavbar">
                    <div class="prev text-left float-left">
                        <a id="back-page" class="hide" href="javascript:;" title="上一页">
                            <span class="glyphicon glyphicon-menu-left jump-icon" aria-hidden="true"></span>
                        </a>
                    </div>
                    <div class="next text-right float-right">
                        <a id="forward-page" class="hide" href="javascript:;" title="下一页">
                            <span class="glyphicon glyphicon-menu-right jump-icon" aria-hidden="true"></span>
                        </a>
                    </div>
                    <div class="up">
                        @yield('manualnavbar-up')
                    </div>
                </div>
            </nav>
        </div>
        @yield('content')
    </div>

    <div class="phpdoc-nav-bottom">
        <nav class="navbar navbar-default navbar-fixed-bottom">
            <div class="container-fluid">
                <form class="navbar-form navbar-left" id="search-form" role="search" method="get" action="/search/form">
                    <div class="input-group has-feedback dropup">
                        <span class="input-group-btn span-home" title="首页">
                            <button class="btn btn-default" type="button" id="jump_home">
                                <span class="glyphicon glyphicon-home" aria-hidden="true"></span>
                            </button>
                            <a href="/" class="hide" id="home_url"></a>
                        </span>

                        <input type="text" class="form-control input-search" placeholder="Search" id="search_input" data-oldval="" autocomplete="off" name="keyword">

                        <span class="input-group-btn span-search" title="搜索">
                            <button class="btn btn-default" type="submit" id="search-submit">
                                <span class="glyphicon glyphicon-search" aria-hidden="true"></span>
                            </button>
                        </span>
                        <ul class="dropdown-menu" id="search_auto_complete_box">

                        </ul>
                    </div>
                </form>
            </div>
        </nav>
        <div id="topAffix" class="" title="返回顶部">
            <span class="glyphicon glyphicon-plane span-icon" aria-hidden="true"></span>
        </div>
        <input type="hidden" name="file_name" id="file_name" value="@yield('file_name')">
    </div>
</div>

<script src="/js/app.js"></script>
<script>window._bd_share_config={"common":{"bdSnsKey":{},"bdText":"","bdMini":"2","bdMiniList":false,"bdPic":"","bdStyle":"0","bdSize":"16"},"slide":{"type":"slide","bdImg":"0","bdPos":"right","bdTop":"100"}};with(document)0[(getElementsByTagName('head')[0]||body).appendChild(createElement('script')).src='https://bdimg.share.baidu.com/static/api/js/share.js?v=89860593.js?cdnversion='+~(-new Date()/36e5)];</script>
</body>
</html>
