<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="keywords" content="PHP手册,PHP中文手册,PHP中文参考手册,PHP7手册,PHP7中文手册,PHP7中文参考手册,PHP最新手册,PHP最新中文手册,PHP最新中文参考手册">
    <meta name="baidu-site-verification" content="KkdffVUonZ"/>
    <title>@yield('title') - 管理系统</title>
    <link rel="stylesheet" href="//apps.bdimg.com/libs/nprogress/0.2.0/nprogress.min.css">
    <link rel="stylesheet" href="/css/font-awesome.min.css">
    <link rel="stylesheet" href="/vendor/layui/css/layui.css" media="all">
    @yield('link')
    @if(env('APP_DEBUG'))
        <link rel="stylesheet" href="/css/admin.css?time={{time()}}">
    @else
        <link rel="stylesheet" href="/css/admin.css">
    @endif

    <script>
        var GV = {
            ROUTE: "{{$route}}"
        };
    </script>
    <script src="//apps.bdimg.com/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="//cdn.bootcss.com/jquery.pjax/2.0.1/jquery.pjax.min.js"></script>
    <script src="//apps.bdimg.com/libs/nprogress/0.2.0/nprogress.min.js"></script>
    <script src="/vendor/layui/layui.js"></script>

</head>
<body>

<div class="layui-layout layui-layout-admin">
    <div class="layui-header header header-doc">
        <div class="layui-main">
            <a class="logo" href="/">
                <img src="/images/logo.png" title="{{config('app.name')}}" alt="{{config('app.name')}}">
            </a>

            <ul class="layui-nav" lay-filter="">
                <li class="layui-nav-item layui-this" title="首页" data-route-name="admin_index">
                    <a href="{{url('/admin/index')}}" ><i class="fa fa-home"></i></a>
                </li>
                <li class="layui-nav-item" title="文档管理" data-route-name="admin_document">
                    <a href="{{url('/admin/document')}}" ><i class="fa fa-money"></i></a>
                </li>
                <li class="layui-nav-item" title="统计" data-route-name="admin_statistics">
                    <a href="{{url('/admin/statistics')}}" ><i class="fa fa-line-chart"></i></a>
                </li>
                <li class="layui-nav-item" title="设置" data-route-name="admin_setups">
                    <a href="{{url('/admin/setups')}}" ><i class="fa fa-cogs"></i></a>
                </li>
            </ul>

            <div class="user-action">
                <ul>
                    <li>
                        <a href="javascript:;" id="logout" title="注销" data-token="{{csrf_token()}}">
                            <i class="fa fa-sign-out"></i>
                        </a>
                    </li>
                </ul>
            </div>

        </div>
    </div>
    <div class="layui-tab-content" id="pjax-container">
        @yield('content')
    </div>
</div>

<script>
    $('.layui-header .layui-nav').find('li.layui-nav-item').removeClass('layui-this');
    $('.layui-header .layui-nav').find('li.layui-nav-item[data-route-name="' + GV.ROUTE + '"]').addClass('layui-this');

    layui.use(['element', 'layer'], function () {
        var element = layui.element;

        //注销
        var beforeSendMsgIndex = '';
        $('#logout').click(function () {
            var $this = $(this);
            layer.confirm('确认注销？', {
                icon: 3,
                title: '提示'
            }, function (index) {
                $.ajax({
                    'url': '{{@url("/logout")}}',
                    'type': 'post',
                    'dataType': 'json',
                    'data': {'_token': $this.data('token')},
                    'beforeSend': function () {
                        beforeSendMsgIndex = layer.msg('注销中…', {
                            icon: 16,
                            shade: 0.01,
                            time: 0 //不自动关闭
                        });
                    },
                    'success': function (data) {
                        if (data.code == 1) {
                            layer.msg('注销成功！', {
                                icon: 6,
                                time: 1000
                            }, function(){
                                window.location.reload();
                            });
                        } else {
                            layer.msg(data.msg, {
                                icon: 5
                            });
                        }
                    },
                    'complete': function () {
                        //关闭提示框
                        layer.close(beforeSendMsgIndex);
                    }
                })
            })
        })

        $(document).pjax('.layui-header .layui-nav-item a', '#pjax-container');

        $(document).on("pjax:timeout", function (event) {
            // 阻止超时导致链接跳转事件发生
            event.preventDefault()
        });

        $(document).on('pjax:start', function () {
            NProgress.start();
        });

        $(document).on('pjax:end', function () {
            NProgress.done();
        });
    });
</script>
</body>
</html>
