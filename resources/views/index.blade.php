@extends('layouts.app')

@section('title', $info['title'])
@section('file_name', isset($info['file_name']) ? $info['file_name'] : '')
@section('sidebar')

@endsection

@section('content')

    {{--<hr/>--}}
    @if (isset($info['content']))
        {!! ($info['content']) !!}
    @else
        <section class="text-center">
            <h1>404 Not Found</h1>
        </section>
    @endif
    <hr/>

@endsection

@section('manualnavbar-up')
    @if (isset($info['up_info']))
        <a href="{{$info['up_info']['file_name']}}" title="{{$info['up_info']['title']}}">
            <span class="glyphicon glyphicon-menu-hamburger jump-icon" aria-hidden="true"></span>
        </a>
    @endif
@endsection