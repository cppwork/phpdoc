<!DOCTYPE html>
<html lang="zh-CN">

<head>
    <meta charset="UTF-8">
    <title>登录 - {{@config('app.name')}}</title>
    <link rel="shortcut icon" href="/favicon.ico"/>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="format-detection" content="telephone=no">
    <link rel="stylesheet" href="/vendor/layui/css/layui.css" media="all">
    @if(env('APP_DEBUG'))
        <link rel="stylesheet" href="/css/login.css?time={{time()}}">
    @else
        <link rel="stylesheet" href="/css/login.css">
    @endif
</head>

<body>
<div class="layui-layout layui-layout-admin container">
    <span class="line"></span>
    <div class="box">
        <div class="login-box">
            <header>
                <h3>{{strtoupper(@config('app.name'))}}管理系统</h3>
            </header>
            <section>

                <div class="layui-tab layui-tab-card" lay-filter="menu-tab">
                    <ul class="layui-tab-title">
                        <li class="layui-this">账号登录</li>
                        <li>邮箱登录</li>
                    </ul>
                    <div class="layui-tab-content">

                        <div class="layui-tab-item layui-show" style="height: 210px;">
                            <form action="/admin/login" method="post" class="layui-form">
                                <div class="layui-form-item">
                                    <label class="layui-form-label">账号</label>
                                    <div class="layui-input-block">
                                        <input type="text" class="layui-input" name="account" id="account" required lay-verify="required" placeholder="用户名/手机号码/邮箱"
                                        />
                                    </div>
                                </div>
                                <div class="layui-form-item">
                                    <label class="layui-form-label">密码</label>
                                    <div class="layui-input-block">
                                        <input type="password" name="password" id="password" required lay-verify="required" placeholder="登录密码" autocomplete="off"
                                               class="layui-input">
                                    </div>
                                </div>
                                <div class="layui-form-item">
                                    <div class="fn-left">
                                        <label class="layui-form-label">验证码</label>
                                        <div class="layui-input-inline" style="width: 130px;">
                                            <input type="text" class="layui-input" name="captcha" id="captcha" required lay-verify="required" placeholder="验证码"/>
                                        </div>
                                    </div>
                                    <div class="fn-right">
                                        <img src="{{@captcha_src('flat')}}" class="verify-img" alt="看不清？点击刷新" onclick="this.src='{{@captcha_src('flat')}}?'+Math.random()">
                                    </div>
                                </div>
                                <div class="layui-form-item">
                                    <div class="layui-input-block">
                                        {{csrf_field()}}
                                        <input type="hidden" name="login_type" value="1">
                                        <button class="layui-btn" lay-submit lay-filter="accountLoginType">登录</button>
                                    </div>
                                </div>
                            </form>
                        </div>

                        <div class="layui-tab-item" style="">
                            <form action="/admin/login" method="post" class="layui-form">
                                <div class="layui-form-item">
                                    <label class="layui-form-label">邮 箱</label>
                                    <div class="layui-input-block">
                                        <input type="text" class="layui-input" name="email" id="email" required lay-verify="required" placeholder="邮箱"/>
                                    </div>
                                </div>
                                <div class="layui-form-item">
                                    <div class="fn-left">
                                        <label class="layui-form-label">验证码</label>
                                        <div class="layui-input-inline" style="width: 130px;">
                                            <input type="text" class="layui-input layui-disabled" name="email_code" id="email_code" required lay-verify="required" placeholder="验证码" disabled/>
                                        </div>
                                    </div>
                                    <div class="fn-right">
                                        <button type="button" class="layui-btn" id="sendEmailCode">发送邮箱验证码</button>
                                    </div>
                                </div>
                                <div class="layui-form-item">
                                    <div class="layui-input-block">
                                        {{csrf_field()}}
                                        <input type="hidden" name="login_type" value="2">
                                        <button class="layui-btn layui-btn-disabled" lay-submit lay-filter="emailLoginType" id="emailLoginBtn" disabled>登录</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>

    <div class="row footer">
        <p>Copyright © 2015 -
            <script>
                var dateHandle = new Date();
                document.write(dateHandle.getFullYear());
            </script>
            &nbsp;
            <a href="{{@config('app.url')}}" target="_blank">
                {{@config('app.name')}}
            </a>
            &nbsp;
            All Rights Reserved <a href="http://www.miitbeian.gov.cn/" target="_blank">粤ICP备16032168号-2</a>
        </p>
    </div>
</div>

<!--JS引用-->
<script src="/js/jquery.min.js"></script>
<script src="/vendor/layui/layui.js"></script>
<script src="/js/Particleground.js"></script>
<script>
    layui.use(['element', 'form'], function () {
        var layer = layui.layer,
            form = layui.form;

        //粒子背景特效
        $('body').particleground({
            dotColor: '#E8DFE8',
            lineColor: '#133b88'
        });

        //登录发送邮件验证码倒计时
        var COUNT_TIME = 60,
            LOGIN_EMAIL_COUNT = COUNT_TIME;

        //账号登录提交
        form.on('submit(accountLoginType)', function (data) {
            var beforeSendMsgIndex = '';
            $.ajax({
                'url': data.form.action,
                'type': 'post',
                'dataType': 'json',
                'data': data.field,
                'beforeSend': function () {
                    beforeSendMsgIndex = layer.msg('处理中…', {
                        icon: 16,
                        shade: 0.01,
                        time: 0 //不自动关闭
                    });
                },
                'success': function (data) {
                    if (data.code == 1) {
                        layer.msg('登录成功！', {
                            icon: 6,
                            time: 1000
                        }, function () {
                            window.location.href = '{{@url("/admin/index")}}';
                        })
                    } else {
                        layer.msg(data.msg, {
                            icon: 5
                        }, function () {
                            $('#captcha').val('');
                            $('.verify-img').trigger('click');
                        })
                    }
                },
                'complete': function () {
                    //关闭提示框
                    layer.close(beforeSendMsgIndex);
                }
            })
            return false;
        })

        //邮箱验证码登录提交
        form.on('submit(emailLoginType)', function (data) {
            var beforeSendMsgIndex = '';
            $.ajax({
                'url': data.form.action,
                'type': 'post',
                'dataType': 'json',
                'data': data.field,
                'beforeSend': function () {
                    beforeSendMsgIndex = layer.msg('处理中…', {
                        icon: 16,
                        shade: 0.01,
                        time: 0 //不自动关闭
                    });
                },
                'success': function (data) {
                    if (data.code == 1) {
                        layer.msg('登录成功！', {
                            icon: 6,
                            time: 1000
                        }, function () {
                            window.location.href = '{{@url("/admin/index")}}';
                        })
                    } else {
                        layer.msg(data.msg, {
                            icon: 5
                        }, function () {
                            $('#email_code').val('');
                        })
                    }
                },
                'complete': function () {
                    //关闭提示框
                    layer.close(beforeSendMsgIndex);
                }
            })
            return false;
        })

        //发送邮件登录验证码
        $('#sendEmailCode').click(function () {
            if (LOGIN_EMAIL_COUNT != COUNT_TIME) {
                return false;
            }

            var $this = $(this),
                $email = $('#email');
            if (!$email.val()) {
                $email.addClass('layui-form-danger').focus();
                return false;
            }

            $.ajax({
                'url': '{{@url("/admin/send_email_code")}}',
                'dataType': 'json',
                'data': {'email': $email.val(), '_token': $('input[name="_token"]').val()},
                'type': 'post',
                'beforeSend': function () {
                    $('#sendEmailCode').text('正在发送邮件…').addClass('layui-btn-disabled').attr('disabled', true);
                },
                'success': function (data) {
                    if (data.code == 1) {
                        email_count_down();
                        $('#email_code').removeClass('layui-disabled').attr('disabled', false).focus();
                        $('#emailLoginBtn').removeClass('layui-btn-disabled').attr('disabled', false);
                    } else {
                        layer.msg(data.msg, {icon: 5});
                        $('#sendEmailCode').text('发送验证码').removeClass('layui-btn-disabled').attr('disabled', false);
                    }
                }
            })
        })

        /**
         * 邮件验证码倒计时
         * @returns {boolean}
         */
        function email_count_down() {
            if (LOGIN_EMAIL_COUNT == 0) {
                $('#sendEmailCode').text('发送验证码').removeClass('layui-btn-disabled').attr('disabled', false);
                LOGIN_EMAIL_COUNT = COUNT_TIME;
                return false;
            }
            window.setTimeout(function () {
                $('#sendEmailCode').text(LOGIN_EMAIL_COUNT + '秒后可发送');
                LOGIN_EMAIL_COUNT = parseInt(LOGIN_EMAIL_COUNT) - 1;
                email_count_down();
            }, 1000);
        }
    });
</script>
</body>

</html>