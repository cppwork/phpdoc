<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="keywords" content="PHP手册,PHP中文手册,PHP中文参考手册,PHP7手册,PHP7中文手册,PHP7中文参考手册,PHP最新手册,PHP最新中文手册,PHP最新中文参考手册">
    <meta name="baidu-site-verification" content="KkdffVUonZ"/>
    <title>{{$title}} - 管理系统</title>
    <link rel="stylesheet" href="//apps.bdimg.com/libs/nprogress/0.2.0/nprogress.min.css">
    <link rel="stylesheet" href="/css/font-awesome.min.css">
    <link rel="stylesheet" href="/vendor/layui/css/layui.css" media="all">

    @if(env('APP_DEBUG'))
        <link rel="stylesheet" href="/css/admin.css?time={{time()}}">
    @else
        <link rel="stylesheet" href="/css/admin.css">
    @endif

    <script>
        var GV = {
            ROUTE: "{{$route}}"
        };
    </script>

    <script src="//apps.bdimg.com/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="//cdn.bootcss.com/jquery.pjax/2.0.1/jquery.pjax.min.js"></script>
    <script src="//apps.bdimg.com/libs/nprogress/0.2.0/nprogress.min.js"></script>
    <script src="/vendor/layui/layui.js"></script>
</head>
<body>
<div class="layui-box" style="margin: 10px 10px 10px 0;">
    <form class="layui-form" action="{{route('admin_document_edit')}}">
        <div class="layui-form-item">
            <label class="layui-form-label">文件名</label>
            <div class="layui-input-block">
                <input type="text" name="file_name" required lay-verify="required" placeholder="请输入文件名" value="{{$info['file_name']}}" autocomplete="off" class="layui-input">
            </div>
        </div>

        <div class="layui-form-item">
            <label class="layui-form-label">标题</label>
            <div class="layui-input-block">
                <input type="text" name="title" required lay-verify="required" placeholder="请输入标题" value="{{$info['title']}}" autocomplete="off" class="layui-input">
            </div>
        </div>

        <div class="layui-form-item">
            <label class="layui-form-label">关键字</label>
            <div class="layui-input-block">
                <input type="text" name="keyword" required lay-verify="required" placeholder="请输入关键字" value="{{$info['keyword']}}" autocomplete="off" class="layui-input">
            </div>
        </div>

        <div class="layui-form-item layui-form-text">
            <label class="layui-form-label">内容</label>
            <div class="layui-input-block" style="margin-right: 10px;">
                <script id="content" name="content" type="text/plain">{!! $info['content'] !!}</script>
            </div>
        </div>
        <div class="layui-form-item">
            <div class="layui-input-block">
                <button class="layui-btn" lay-submit lay-filter="saveEdit">保存</button>
            </div>
        </div>
        <input type="hidden" name="id" value="{{$info['id']}}">
        {{csrf_field()}}
    </form>
</div>


<!-- 配置文件 -->
<script type="text/javascript" src="/vendor/ueditor/ueditor.config.js"></script>
<!-- 编辑器源码文件 -->
<script type="text/javascript" src="/vendor/ueditor/ueditor.all.js"></script>
<!-- 实例化编辑器 -->
<script type="text/javascript">
    var ue = UE.getEditor('content');
</script>
<script>
    $('.layui-header .layui-nav').find('li.layui-nav-item').removeClass('layui-this');
    $('.layui-header .layui-nav').find('li.layui-nav-item[data-route-name="' + GV.ROUTE + '"]').addClass('layui-this');

    layui.use(['element', 'layer', 'form'], function () {
        var element = layui.element,
            form = layui.form,
            loading_index = '';

        //监听提交
        form.on('submit(saveEdit)', function(data){
            $.ajax({
                'type': 'post',
                'dataType': 'json',
                'url': data.form.action,
                'data': data.field,
                'beforeSend': function () {
                    loading_index = beforeSendRender();
                },
                'success': function (res) {
                    if (res.code == 1) {
                        layer.msg('保存成功！', {icon: 6, time: 2000}, function () {
                            parent.location.reload();
                        });
                    } else {
                        layer.msg(res.msg, {icon: 5, time: 5000});
                    }
                },
                'complete': function () {
                    layer.close(loading_index);
                }

            })
            return false;
        });

        /**
         * 异步加载层
         */
        function beforeSendRender() {
            return layer.load(0, {
                shade: 0.1
            }); //0代表加载的风格，支持0-2
        }
    });
</script>
</body>
</html>