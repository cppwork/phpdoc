@extends('layouts.admin')

@section('title', $title)

@section('link')

@endsection

@section('content')
    <div class="layui-box" style="max-width: 800px;">
        <form class="layui-form layui-form-pane" method="post">
            <div class="layui-form-item">
                <label class="layui-form-label" style="">网站名称</label>
                <div class="layui-input-block">
                    <input type="text" name="web_name" required lay-verify="required" placeholder="网站名称" value="{{$info['web_name']}}" autocomplete="off" class="layui-input">
                </div>
            </div>

            <div class="layui-form-item">
                <label class="layui-form-label">网站URL</label>
                <div class="layui-input-block">
                    <input type="text" name="web_url" required lay-verify="required" placeholder="网站URL" value="{{$info['web_url']}}" autocomplete="off" class="layui-input">
                </div>
            </div>

            <div class="layui-form-item">
                <label class="layui-form-label">版本号</label>
                <div class="layui-input-inline">
                    <input type="text" name="version" required lay-verify="required" placeholder="版本号" value="{{$info['version']}}" autocomplete="off" class="layui-input">
                </div>
            </div>

            <div class="layui-form-item">
                <label class="layui-form-label" title="文档最后更新时间">文档最后更新时间</label>
                <div class="layui-input-inline">
                    <input class="layui-input" name="last_update_time" value="{{$info['last_update_time']}}" placeholder="文档最后更新时间" onclick="layui.laydate({elem: this, istime: false, format: 'YYYY-MM-DD'})">
                </div>
            </div>

            <div class="layui-form-item">
                <div class="layui-input-block">
                    <button class="layui-btn" lay-submit lay-filter="saveEdit">保存</button>
                </div>
            </div>
            <input type="hidden" name="id" value="{{$info['id']}}">
            {{csrf_field()}}
        </form>
    </div>

    <script>
        layui.use(['layer', 'form', 'laydate'], function () {
            var layer = layui.layer,
                form = layui.form;

            form.on('submit(saveEdit)', function(data){
                $.ajax({
                    'type': 'post',
                    'dataType': 'json',
                    'url': data.form.action,
                    'data': data.field,
                    'beforeSend': function () {
                        loading_index = beforeSendRender();
                    },
                    'success': function (res) {
                        if (res.code == 1) {
                            layer.msg('保存成功！', {icon: 6, time: 2000}, function () {
                                parent.location.reload();
                            });
                        } else {
                            layer.msg(res.msg, {icon: 5, time: 5000});
                        }
                    },
                    'complete': function () {
                        layer.close(loading_index);
                    }

                })
                return false;
            });

            /**
             * 异步加载层
             */
            function beforeSendRender() {
                return layer.load(0, {
                    shade: 0.1
                }); //0代表加载的风格，支持0-2
            }
        })
    </script>
@endsection