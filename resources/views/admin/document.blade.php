@extends('layouts.admin')

@section('title', $title)

@section('link')
    <style>
        table tr th{ color:#0f0f0f !important; font-weight: bold !important;}
        table tr td{ color: #0f0f0f !important;}
    </style>
@endsection

@section('content')

    <form class="layui-form">
        <div class="layui-form-item">
            <div class="layui-inline">
                <label class="layui-form-label">文件名称：</label>
                <div class="layui-input-inline">
                    <input type="text" name="file_name" value="{{request('file_name')}}" placeholder="文件名称" autocomplete="off" class="layui-input">
                </div>
            </div>

            <div class="layui-inline">
                <label class="layui-form-label">标题</label>
                <div class="layui-input-inline">
                    <input type="text" name="title" value="{{request('title')}}" placeholder="标题" autocomplete="off" class="layui-input">
                </div>
            </div>

            <div class="layui-inline">
                <label class="layui-form-label">关键字</label>
                <div class="layui-input-inline">
                    <input type="text" name="keyword" value="{{request('keyword')}}" placeholder="关键字" autocomplete="off" class="layui-input">
                </div>
            </div>

            <div class="layui-inline" style="float: right;">
                <button class="layui-btn" lay-submit lay-filter="searchSubmit" title="筛选">
                    <i class="layui-icon">&#xe615;</i></button>
                <button class="layui-btn" type="reset"   title="清空">
                    <i class="fa fa-eraser"></i></button>
            </div>
        </div>
    </form>

    <table class="layui-table" lay-size="sm">
        <colgroup>
            <col width="80">
            <col width="">
            <col width="">
            <col width="">
            {{--<col width="100">--}}
            <col width="100">
            <col width="180">
            <col width="180">
            <col width="180">
            <col width="100">
        </colgroup>
        <thead>
        <tr>
            <th>序号</th>
            <th>文件名称</th>
            <th>标题</th>
            <th>关键字</th>
            {{--<th>父级</th>--}}
            <th >浏览次数</th>
            <th>添加时间</th>
            <th>更新时间</th>
            <th>同步时间</th>
            <th>操作</th>
        </tr>
        </thead>
        <tbody>
        @foreach($list as $key=>$val)
            <tr data-id="{{$val['id']}}">
                <td>{{($p - 1) * $limit + $key + 1}}</td>
                <td><a href="{{url($val['file_name'])}}" target="_blank">{{$val['file_name']}}</a></td>
                <td>{{$val['title']}}</td>
                <td>{{$val['keyword']}}</td>
                {{--<td>{{$val['up_id']}}</td>--}}
                <td>{{$val['hits']}}</td>
                <td>{{$val['create_time']}}</td>
                <td>{{$val['update_time']}}</td>
                <td>{{$val['sync_time']}}</td>
                <td>
                    <div class="layui-btn-group">
                        <button class="layui-btn layui-btn-sm edit">
                            <i class="layui-icon">&#xe642;</i>
                        </button>
                    </div>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    <div id="page"></div>
    <script>
        layui.use(['laypage', 'layer', 'table'], function () {
            var laypage = layui.laypage,
                table = layui.table,
                layer = layui.layer;

            laypage.render({
                elem: 'page',
                curr: '{{$p}}',
                limit: '{{$limit}}',
                count: '{{$count}}', //总页数
                groups: 10, //连续显示分页数
                layout: ['prev', 'page', 'next', 'count'],
                skip: false,
                jump: function (obj, first) {
                    var href = window.location.href.split('?'), url = href[0];
                    $('#page a').each(function (i, v) {
                        var p = $(v).data('page');
                        $(v).attr('href', url + "?p=" + p + "&{!! $url_param_str !!}");
                    })
                }
            });

            $(document).pjax('.layui-laypage a', '#pjax-container');

            //编辑弹窗
            $('.edit').click(function () {
                var $this = $(this), $tr = $this.closest('tr'), id = $tr.data('id');

                layer.open({
                    type: 2,
                    title: '编辑',
                    shadeClose: false,
                    skin: 'layui-layer-rim', //加上边框
                    shade: 0.1,
                    anim: 5,
                    area: ['85%', '85%'],
                    content: '{{route("admin_document_edit")}}?id=' + id,
                    cancel: function (index, layero) {

                    }
                });
            })

        })
    </script>
@endsection