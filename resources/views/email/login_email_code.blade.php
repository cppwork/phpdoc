<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <meta charset="utf-8">
    <title>邮箱登录验证码</title>
    <style>
        html, body {
            font-family: "Helvetica Neue", Helvetica, Microsoft Yahei, Hiragino Sans GB, WenQuanYi Micro Hei, sans-serif;
            letter-spacing: 1px;
        }
    </style>
</head>
<body>

<div style="margin: 0 auto; width:700px;">
    <div style="width:680px; padding:10px; border-top:1px dotted #ccc; margin-top: 30px;">
        <div style="line-height:1.5; font-size:14px; margin-bottom:25px; color:#4d4d4d;">
            <p style="display:block;margin-bottom:15px;">
                亲爱的用户，<span style="color:#f60;font-size: 16px;"></span>您好：
            </p>

            <p style="display:block; margin-bottom:15px; margin-left: 30px;">
                您正在登录【{{@config('app.name')}}】，请在验证码输入框中输入：
                <span style="color:#f60;font-size: 24px"><span style="border-bottom:1px dashed #ccc;z-index:1" onclick="return false;" data="{{$email_code}}">{{$email_code}}</span></span>，以完成登录操作。
            </p>

        </div>
    </div>
    <div style="width:100%;margin:0 auto;">
        <div style="padding:10px 10px 0;border-top:1px dotted #ccc;color:#747474;margin-bottom:20px;line-height:1.3em;font-size:12px;">
            <p>此为系统邮件，请勿回复<br>
                请保管好您的邮箱，避免账号被他人盗用
            </p>
        </div>
    </div>
</div>
</body>
</html>