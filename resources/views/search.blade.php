@extends('layouts.app')

@section('title', $title)
@section('sidebar')

@endsection

@section('content')
    <div class="set" id="search">
        <h1 class="title">{{$keyword}}</h1>

        <ul class="chunklist chunklist_set">
            @foreach($list as $key=>$val)
                <li>
                    <a href="/{{$val->file_name}}">{{$val->keyword}}</a>
                    @if($val->keyword != $val->title)
                     - {{$val->title}}
                    @endif
                </li>
            @endforeach
        </ul>
    </div>
@endsection

@section('manualnavbar-up')
    <div class="up">
        <p class="text-muted" style="color: #fff; font-weight: bold;">搜索到相关结果{{$count}}条</p>
    </div>
@endsection