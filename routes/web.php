<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//文档
Route::get('/{name?}', 'DocumentController@index');

//搜索 - ajax方式
Route::get('/search/ajax/keyword/{keyword}', 'DocumentController@searchAjax');

//搜索 - form方式
Route::get('/search/form', 'DocumentController@searchForm');

//更新浏览次数
Route::get('/hits/name/{file_name?}', 'DocumentController@updatehits');

Route::get('/jieba/seg/{seg}', 'DocumentController@jieba_demo');

Route::get('/phpanalysis/seg/{seg}', 'DocumentController@phpanalysis_demo');

//钩子
Route::group(['prefix' => 'webhooks'], function () {
    //码云 - 拉取最新代码
    Route::post('gitee', 'WebhooksController@gitee');
});

//管理后台
Route::group(['prefix' => 'admin'], function () {
    //登录
    Route::match(['get', 'post'], '/login', 'LoginController@login');
    //注销
    Route::match(['get', 'post'], '/logout', 'LoginController@logout');
    //发送登录邮件验证码
    Route::post('/send_email_code', 'LoginController@sendEmailCode');

    Route::group(['middleware' => 'AdminAuth'], function () {
        //首页
        Route::get('/index', 'AdminController@index')->name('admin_index');

        //文档列表
        Route::get('/document', 'AdminController@document')->name('admin_document');
        //编辑保存文档
        Route::match(['get', 'post'], '/document_edit', 'AdminController@document_edit')->name('admin_document_edit');

        //统计
        Route::get('/statistics', 'AdminController@statistics')->name('admin_statistics');

        //设置
        Route::match(['get', 'post'], '/setups', 'AdminController@setups')->name('admin_setups');

    });
});
